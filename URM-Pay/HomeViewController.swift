//
//  HomeViewController.swift
//  URM-Pay
//
//  Created by Jiaxin Li on 11/28/18.
//  Copyright © 2018 URM. All rights reserved.
//

import Foundation
import UIKit
import AWSMobileClient
import Alamofire
import SwiftyJSON
import AWSCore
import AWSPinpoint

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var nameOrInstructionLabel: UILabel!
    @IBOutlet weak var signInOrSignOut: UIBarButtonItem!
    
    var isUserSignedIn = false
    var parkingSessionsArray = [[String:AnyObject]]()
    
    func initAWSClient() {
        AWSMobileClient.sharedInstance().initialize { (userState, error) in
            if let userState = userState {
                switch(userState){
                case .signedIn:
                    self.isUserSignedIn = true
                    self.configureUIOnUserState(isUserSignedIn: self.isUserSignedIn)
                case .signedOut:
                    self.isUserSignedIn = false
                    self.configureUIOnUserState(isUserSignedIn: self.isUserSignedIn)
                    self.showSignInPage()
                default:
                    print("Unknown Error")
                }
            } else if let error = error {
                print("error: \(error.localizedDescription)")
            }
        }
    }
    
    func showSignInPage() {
        let logoImage = #imageLiteral(resourceName: "MyCustomLogo")
        AWSMobileClient.sharedInstance().showSignIn(navigationController: self.navigationController!, signInUIOptions: SignInUIOptions(canCancel: true,
                            logoImage: logoImage,
                            backgroundColor: UIColor.black))
            { (signInState, error) in
            if let signInState = signInState {
                print("logged in!")
                self.isUserSignedIn = true
                self.configureUIOnUserState(isUserSignedIn: self.isUserSignedIn)
                //self.performSegue(withIdentifier: "loginToHome", sender: self)
            } else {
                print("error logging in: \(error?.localizedDescription)")
            }
        }
    }
    
    func fetchParkingSessions() {
        Alamofire.request("https://dr0539s4c6.execute-api.us-east-1.amazonaws.com/getTest/parking-sessions", method:.get).responseJSON { (responseData) -> Void in
            if let rawData = responseData.result.value {
                let json = JSON(rawData)
                if let swiftyJsonArray = json["body"].arrayObject {
                    self.parkingSessionsArray = swiftyJsonArray as! [[String:AnyObject]]
                    if self.parkingSessionsArray.count > 0 {
                        DispatchQueue.main.async {
                            self.uiTableView.reloadData()
                        }
                    }
                } else {
                    print("error converting json")
                }
            }
        }
    }
    
    func configureUIOnUserState(isUserSignedIn: Bool) {
        if isUserSignedIn {
            self.uiTableView.isHidden = false
            self.signInOrSignOut.title = "sign out"
            self.nameOrInstructionLabel.text = AWSMobileClient.sharedInstance().username
        } else {
            self.uiTableView.isHidden = true
            self.signInOrSignOut.title = "sign in"
            self.nameOrInstructionLabel.text = "please sign in to continue"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.parkingSessionsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "session") as! ParkingSessionTableViewCell
        let dealItem = self.parkingSessionsArray[indexPath.row]
        cell.licensePlateLabel.text = dealItem["License_Plate"] as? String
        cell.timeInLabel.text = dealItem["time_In"] as? String
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.00
    }
    
    @IBAction func didPressSignOut(_ sender: Any) {
        if self.isUserSignedIn {
            AWSMobileClient.sharedInstance().signOut()
            self.isUserSignedIn = false
            self.configureUIOnUserState(isUserSignedIn: self.isUserSignedIn)
        } else {
            self.showSignInPage()
        }
    }
    
    @IBOutlet weak var uiTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.uiTableView.delegate = self
        self.uiTableView.dataSource = self
    
        //init aws mobile client
        self.initAWSClient()
        self.fetchParkingSessions()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
}
