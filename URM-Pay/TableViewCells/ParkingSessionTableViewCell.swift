//
//  ParkingSessionTableViewCell.swift
//  URM-Pay
//
//  Created by Jiaxin Li on 12/5/18.
//  Copyright © 2018 URM. All rights reserved.
//

import UIKit

class ParkingSessionTableViewCell: UITableViewCell {

    @IBOutlet weak var licensePlateLabel: UILabel!
    @IBOutlet weak var timeInLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
